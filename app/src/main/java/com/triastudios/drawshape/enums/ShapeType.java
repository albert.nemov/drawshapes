package com.triastudios.drawshape.enums;

public enum ShapeType {
    Rect,
    Circle,
    Triangle
}
