package com.triastudios.drawshape.enums;

public enum MotionType {
    Draw,
    Move
}
