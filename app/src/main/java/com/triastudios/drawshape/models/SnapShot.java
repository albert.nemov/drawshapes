package com.triastudios.drawshape.models;

import com.triastudios.drawshape.interfaces.IShape;

import java.util.LinkedList;

public class SnapShot {

    private State state;

    private LinkedList<IShape> points;
    private LinkedList<IShape> figures;

    public SnapShot(State state) {
        this.state = state;
        this.points = new LinkedList<>();
        for (IShape shape : state.getPoints()){
            this.points.add(shape.clone());
        }
        this.figures = new LinkedList<>();
        for (IShape shape : state.getFigures()){
            this.figures.add(shape.clone());
        }
    }

    public void restore(){
        state.setFigures(figures);
        state.setPoints(points);
    }
}
