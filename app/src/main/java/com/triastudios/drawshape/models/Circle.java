package com.triastudios.drawshape.models;

import android.graphics.Canvas;

import androidx.annotation.NonNull;

import com.triastudios.drawshape.interfaces.IShape;

public class Circle extends BaseShape {

    private Point center;
    private float radius;

    public Circle(Point center, float radius) {
        this.center = center;
        this.radius = radius;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);
    }

    @Override
    public boolean inFigure(Point point) {
        return ((center.getX() - point.getX()) * (center.getX() - point.getX()) +
                (center.getY() - point.getY()) * (center.getY() - point.getY())) <=
                 radius * radius;
    }

    @Override
    public void move(Point startPoint, Point curPoint) {
        center = curPoint;
    }

    @Override
    public IShape clone() {
        Circle circle = new Circle((Point) center.clone(), radius);
        circle.setPaint(paint);
        return circle;
    }
}
