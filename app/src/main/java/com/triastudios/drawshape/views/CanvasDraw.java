package com.triastudios.drawshape.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.triastudios.drawshape.enums.MotionType;
import com.triastudios.drawshape.enums.ShapeType;
import com.triastudios.drawshape.interfaces.IShape;
import com.triastudios.drawshape.models.Point;
import com.triastudios.drawshape.services.ShapesService;
import com.triastudios.drawshape.services.SnapShotsService;

import java.util.ArrayList;

public class CanvasDraw extends View {

    private Paint paint;
    private MotionType motionType;
    private ArrayList<IShape> moveFigures;

    private Point startMove;

    private ShapesService shapesService;

    public CanvasDraw(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(10);

        this.shapesService = ShapesService.getInstance();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (IShape shape : shapesService.getFigures()){
            shape.draw(canvas);
        }
        for (IShape shape : shapesService.getPoints()){
            shape.draw(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (motionType){
            case Draw :
                onDrawEvent(event);
                break;
            case Move:
                onMoveEvent(event);
                break;
        }

        return true;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    public void onDrawEvent(MotionEvent event){

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            IShape shape = new Point(event.getX(), event.getY());
            shape.setPaint(paint);
            shapesService.setPoint(shape);
            invalidate();
        }

    }

    public void onMoveEvent(MotionEvent event){
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN :
                startMove = new Point(x, y);
                moveFigures = new ArrayList<>();
                SnapShotsService.getInstance().backup();
                for (IShape shape : shapesService.getFigures()){
                    if (shape.inFigure(startMove)){
                        moveFigures.add(shape);
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                for (IShape shape : moveFigures){
                    shape.move(startMove, new Point(x, y));
                    invalidate();
                }
                break;
        }
    }

    public void setShapeType(ShapeType type){
        shapesService.setShapeType(type);
    }

    public void setMotionType(MotionType motionType) {
        this.motionType = motionType;
    }
}
