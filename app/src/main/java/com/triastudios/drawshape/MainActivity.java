package com.triastudios.drawshape;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.view.CollapsibleActionView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.triastudios.drawshape.enums.MotionType;
import com.triastudios.drawshape.enums.ShapeType;
import com.triastudios.drawshape.models.Point;
import com.triastudios.drawshape.services.ShapesService;
import com.triastudios.drawshape.services.SnapShotsService;
import com.triastudios.drawshape.views.CanvasDraw;

public class MainActivity extends AppCompatActivity {

    private Spinner spinnerColor;
    private CanvasDraw canvasDraw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        canvasDraw = findViewById(R.id.canvasDraw);
        spinnerColor = findViewById(R.id.spinner);
        spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String[] choose = getResources().getStringArray(R.array.colorsPicket);
                Paint paint = new Paint();
                paint.setColor(Color.parseColor(choose[i]));
                paint.setStrokeWidth(10);
                canvasDraw.setPaint(paint);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        canvasDraw.setMotionType(MotionType.Draw);
    }

    public void clickRect(View view) {
        canvasDraw.setShapeType(ShapeType.Rect);
    }

    public void clickTriangle(View view) {
        canvasDraw.setShapeType(ShapeType.Triangle);
    }

    public void clickCircle(View view) {
        canvasDraw.setShapeType(ShapeType.Circle);
    }

    public void clickDraw(View view) {
        canvasDraw.setMotionType(MotionType.Draw);
    }

    public void clickMove(View view) {
        canvasDraw.setMotionType(MotionType.Move);
    }

    public void clickUndo(View view) {
        SnapShotsService.getInstance().undo();
        canvasDraw.invalidate();
    }
}