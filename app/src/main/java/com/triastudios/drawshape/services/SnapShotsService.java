package com.triastudios.drawshape.services;

import com.triastudios.drawshape.models.SnapShot;
import com.triastudios.drawshape.models.State;

import java.util.LinkedList;

public class SnapShotsService {

    private static SnapShotsService instance;

    private LinkedList<SnapShot> snapShots;
    private State originState;

    public SnapShotsService(State originState) {
        this.originState = originState;
        snapShots = new LinkedList<>();
    }

    public static SnapShotsService getInstance() {

        if (instance == null){
            instance = new SnapShotsService(ShapesService.getInstance().getState());
        }

        return instance;
    }

    public void backup(){
        snapShots.addLast(new SnapShot(originState));
    }

    public void undo(){
        if (snapShots.size() <= 0){
            return;
        }

        snapShots.getLast().restore();
        snapShots.removeLast();
    }
}
